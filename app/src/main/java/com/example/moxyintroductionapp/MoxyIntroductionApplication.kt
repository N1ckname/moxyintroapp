package com.example.moxyintroductionapp

import android.app.Application
import com.example.moxyintroductionapp.data.FilmsDataSource
import com.example.moxyintroductionapp.data.FilmsRepository

class MoxyIntroductionApplication : Application() {
    private val filmsDataSource by lazy { FilmsDataSource() }
    val repository: FilmsRepository by lazy { FilmsRepository.getInstance(filmsDataSource) }
}