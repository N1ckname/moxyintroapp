package com.example.moxyintroductionapp.data

import com.example.moxyintroductionapp.domain.Film
import io.reactivex.rxjava3.core.Observable

class FilmsDataSource {
    fun getFilms(): Observable<List<Film>> = Observable.just(films)

    private val films: List<Film> = listOf(
        Film(
            id = 1,
            name = "Hello, world 2!",
            year = 2021,
            genre = "horror",
            poster = "file:///android_asset/Hello, world 2!.png",
            description = "Young programmer returns to his duty after sometime being off. At his new place he found relic PC with some odd program on it..."
        ),
        Film(
            id = 2,
            name = "Hello, world!",
            year = 2020,
            genre = "horror",
            poster = "file:///android_asset/Hello, world!.png",
            description = "Young programmer starts his career. He found some strange tutorial online..."
        ),
        Film(
            id = 3,
            name = "Goodbye, world!",
            year = 1990,
            genre = "comedy",
            poster = "file:///android_asset/Goodbye, world!.png",
            description = "They tried to leave it all, but nothing worked. When they decided to conquer the world..."
        ),
        Film(
            id = 4,
            name = "Lost charger",
            year = 2009,
            genre = "drama",
            poster = "file:///android_asset/Lost Charger.png",
            description = "Among dozens of charging standards there was one, which could bring it all. Is he capable to win this fight?"
        ),
        Film(
            id = 5,
            name = "Winner",
            year = 2021,
            genre = "inspirational",
            poster = "file:///android_asset/Winner.png",
            description = "He thinks, he can win. But can he?"
        )
    )
}