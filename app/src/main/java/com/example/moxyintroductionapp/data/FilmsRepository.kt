package com.example.moxyintroductionapp.data

import com.example.moxyintroductionapp.domain.Film
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class FilmsRepository private constructor(private val filmsProvider: FilmsDataSource) {
    private var films: List<Film> = emptyList()

    fun getFilms(): Observable<List<Film>> {
        return filmsProvider.getFilms()
            .subscribeOn(Schedulers.io())
            .map { providedFilms ->
                films = providedFilms
                films
            }
            .onErrorResumeNext {
                films = emptyList()
                Observable.just(films)
            }
    }

    fun filmExists(filmId: Int): Boolean = getSingleFilmOrNull(filmId) != null

    fun getSingleFilm(filmId: Int): Film = getSingleFilmOrNull(filmId) ?: Film()

    private fun getSingleFilmOrNull(filmId: Int): Film? = films.firstOrNull { film -> film.id == filmId }

    companion object {
        @Volatile
        private var INSTANCE: FilmsRepository? = null

        fun getInstance(filmsDataSource: FilmsDataSource): FilmsRepository {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = FilmsRepository(filmsDataSource)
                    INSTANCE = instance
                }

                return instance
            }
        }
    }
}