package com.example.moxyintroductionapp.presentation.filmsFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.moxyintroductionapp.MoxyIntroductionApplication
import com.example.moxyintroductionapp.databinding.FilmsFragmentBinding
import com.example.moxyintroductionapp.domain.Film
import com.example.moxyintroductionapp.presentation.filmsFragment.adapters.FilmsAdapter

class FilmsFragment : MvpAppCompatFragment(), FilmsView {

    @InjectPresenter
    lateinit var presenter: FilmsPresenter
    private lateinit var binding: FilmsFragmentBinding
    private lateinit var filmsAdapter: FilmsAdapter
    private val navController by lazy { findNavController() }

    @ProvidePresenter
    fun createPresenter(): FilmsPresenter =
        FilmsPresenter((requireActivity().application as MoxyIntroductionApplication).repository)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FilmsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupFilmsAdapter()
    }

    private fun setupFilmsAdapter() {
        filmsAdapter = FilmsAdapter(presenter::openFilmDescription)
        binding.films.adapter = filmsAdapter
    }

    override fun openFilmDescription(filmId: Int) {
        navController.navigate(FilmsFragmentDirections.actionFilmsFragmentToFilmDescriptionFragment(filmId))
    }

    override fun showFilms(films: List<Film>) {
        filmsAdapter.submitList(films)
    }
}