package com.example.moxyintroductionapp.presentation.filmsFragment

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.moxyintroductionapp.data.FilmsRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers

@InjectViewState
class FilmsPresenter(private val repository: FilmsRepository) : MvpPresenter<FilmsView>() {

    init {
        loadFilms()
    }

    private fun loadFilms() {
        repository.getFilms()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {films -> viewState.showFilms(films)}
    }

    fun openFilmDescription(filmId: Int) {
        if (repository.filmExists(filmId)) viewState.openFilmDescription(filmId)
    }
}