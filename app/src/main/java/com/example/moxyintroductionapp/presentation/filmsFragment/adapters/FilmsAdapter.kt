package com.example.moxyintroductionapp.presentation.filmsFragment.adapters

import android.net.Uri
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.moxyintroductionapp.R
import com.example.moxyintroductionapp.databinding.FilmItemBinding
import com.example.moxyintroductionapp.domain.Film
import com.example.moxyintroductionapp.inflate

class FilmsAdapter(private val itemClickListener: (itemId: Int) -> Unit): ListAdapter<Film, FilmsAdapter.FilmViewHolder>(FilmDiffCallback())
{
    class FilmViewHolder(v: View, private val itemClickListener: (itemId: Int) -> Unit) :
        RecyclerView.ViewHolder(v), View.OnClickListener {
        private var binding = FilmItemBinding.bind(v)
        private var itemId: Int = 0

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            itemClickListener.invoke(itemId)
        }

        fun bindFilm(film: Film) {
            itemId = film.id
            binding.apply {
                name.text = film.name
                year.text = film.year.toString()
                genre.text = film.genre
            }

            Glide.with(binding.root)
                .load(Uri.parse(film.poster))
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.ic_launcher_foreground)
                        .error(R.drawable.ic_launcher_foreground))
                .into(binding.poster)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmViewHolder {
        val inflatedView = parent.inflate(R.layout.film_item, false)
        return FilmViewHolder(inflatedView, itemClickListener)
    }

    override fun onBindViewHolder(holder: FilmViewHolder, position: Int) {
        val film = getItem(position)
        holder.bindFilm(film)
    }
}

class FilmDiffCallback:  DiffUtil.ItemCallback<Film>() {
    override fun areItemsTheSame(oldItem: Film, newItem: Film) = oldItem == newItem

    override fun areContentsTheSame(oldItem: Film, newItem: Film) = oldItem == newItem
}