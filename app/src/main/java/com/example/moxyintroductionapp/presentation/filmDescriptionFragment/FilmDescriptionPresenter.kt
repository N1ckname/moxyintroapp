package com.example.moxyintroductionapp.presentation.filmDescriptionFragment

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.moxyintroductionapp.data.FilmsRepository

@InjectViewState
class FilmDescriptionPresenter(private val repository: FilmsRepository) : MvpPresenter<FilmDescriptionView>() {
    fun getFilm(filmId: Int)  {
        val film = repository.getSingleFilm(filmId)
        viewState.showFilm(film)
    }
}