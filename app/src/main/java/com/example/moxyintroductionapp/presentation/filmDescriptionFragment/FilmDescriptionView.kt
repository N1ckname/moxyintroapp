package com.example.moxyintroductionapp.presentation.filmDescriptionFragment

import com.arellomobile.mvp.MvpView
import com.example.moxyintroductionapp.domain.Film

interface FilmDescriptionView : MvpView {
    fun showFilm(film: Film)
}