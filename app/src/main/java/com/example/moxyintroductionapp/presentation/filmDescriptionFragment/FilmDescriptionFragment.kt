package com.example.moxyintroductionapp.presentation.filmDescriptionFragment

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.moxyintroductionapp.MoxyIntroductionApplication
import com.example.moxyintroductionapp.R
import com.example.moxyintroductionapp.databinding.FilmDescriptionFragmentBinding
import com.example.moxyintroductionapp.domain.Film

class FilmDescriptionFragment : MvpAppCompatFragment(), FilmDescriptionView {

    private val navigationArguments: FilmDescriptionFragmentArgs by navArgs()

    private lateinit var binding: FilmDescriptionFragmentBinding
    @InjectPresenter
    lateinit var presenter: FilmDescriptionPresenter

    @ProvidePresenter
    fun createPresenter(): FilmDescriptionPresenter =
        FilmDescriptionPresenter((requireActivity().application as MoxyIntroductionApplication).repository)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FilmDescriptionFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.getFilm(navigationArguments.filmId)
    }

    override fun showFilm(film: Film) {
        binding.apply {
            name.text = film.name
            year.text = film.year.toString()
            genre.text = film.genre
            description.text = film.description
        }

        Glide.with(binding.root)
            .load(Uri.parse(film.poster))
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground))
            .into(binding.poster)
    }
}