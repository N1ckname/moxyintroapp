package com.example.moxyintroductionapp.presentation.filmsFragment

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.moxyintroductionapp.domain.Film

interface FilmsView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun openFilmDescription(filmId: Int)
    fun showFilms(films: List<Film>)
}