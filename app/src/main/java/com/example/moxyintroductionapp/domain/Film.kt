package com.example.moxyintroductionapp.domain

data class Film(
    val id: Int = 0,
    val name: String = "",
    val year: Int = 1896,
    val genre: String = "",
    val poster: String = "",
    val description: String = ""
)